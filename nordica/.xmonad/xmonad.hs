-- IMPORTS --

import XMonad
import Data.Monoid
import System.Exit
import XMonad.Hooks.ManageDocks
import XMonad.Util.Run
import XMonad.Util.SpawnOnce

import qualified XMonad.StackSet as W
import qualified Data.Map        as M

-- TERMINAL --
myTerminal      = "alacritty"

-- FOLLOWS MOUSE --
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

-- PASSES CLICK TO WINDOW --
myClickJustFocuses :: Bool
myClickJustFocuses = False

-- WINDOW WIDTH -- 
myBorderWidth   = 2

-- MODKEY --
myModMask       = mod4Mask

-- WORKSPACES --
myWorkspaces    = ["www","vid","dev","sys","doc","VM","chat","mus","gfx"]

-- BORDER COLORS --
myNormalBorderColor  = "#2E3440"
myFocusedBorderColor = "#434C5E"

------------------------------------------------------------------------

-- KEY BINDINGS --
myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $

    -- launch a terminal
    [ ((modm, 	            xK_Return), spawn $ XMonad.terminal conf)

    -- launch icecat
    , ((modm,               xK_b     ), spawn "icecat")

    -- close focused window
    , ((modm, 	            xK_k     ), kill)

     -- Rotate through the available layout algorithms
    , ((modm,               xK_n     ), sendMessage NextLayout)

    --  Reset the layouts on the current workspace to default
    , ((modm, 	            xK_r     ), setLayout $ XMonad.layoutHook conf)

    -- Resize viewed windows to the correct size
    , ((modm,               xK_n     ), refresh)

    -- Move focus to the next window
    , ((modm,               xK_d     ), windows W.focusDown)

    -- Move focus to the previous window
    , ((modm,               xK_u     ), windows W.focusUp  )

    -- Move focus to the master window
    , ((modm,               xK_m     ), windows W.focusMaster  )

    -- Swap the focused window and the master window
    , ((modm,               xK_s     ), windows W.swapMaster)

    -- Swap the focused window with the next window
    , ((modm .|. shiftMask, xK_d     ), windows W.swapDown  )

    -- Swap the focused window with the previous window
    , ((modm .|. shiftMask, xK_u     ), windows W.swapUp    )

    -- Shrink the master area
    , ((modm,               xK_h     ), sendMessage Shrink)

    -- Expand the master area
    , ((modm,               xK_e     ), sendMessage Expand)

    -- Push window back into tiling
    , ((modm,               xK_t     ), withFocused $ windows . W.sink)

    -- Increment the number of windows in the master area
    , ((modm              , xK_comma ), sendMessage (IncMasterN 1))

    -- Deincrement the number of windows in the master area
    , ((modm              , xK_period), sendMessage (IncMasterN (-1)))

    -- Quit xmonad
    , ((modm .|. mod1Mask,   xK_q    ), io (exitWith ExitSuccess))

    -- Restart xmonad
    , ((modm .|. mod1Mask,   xK_r    ), spawn "xmonad --recompile; xmonad --restart")

    -- Run xmessage with a summary of the default keybindings (useful for beginners)
    , ((modm .|. shiftMask, xK_slash ), spawn ("echo \"" ++ help ++ "\" | xmessage -file -"))
    ]

------------------------------------------------------------------------

-- WORKSPACE BINDINGS --
    ++

    [((m .|. modm, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
    ++

    [((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f))
        | (key, sc) <- zip [xK_w, xK_e, xK_r] [0..]
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]


------------------------------------------------------------------------

-- MOUSE BINDINGS --
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))

    ]

------------------------------------------------------------------------

-- LAYOUTS --
myLayout = avoidStruts (tiled ||| Mirror tiled ||| Full)
  where
     -- default tiling algorithm partitions the screen into two panes
     tiled   = Tall nmaster delta ratio

     -- The default number of windows in the master pane
     nmaster = 1

     -- Default proportion of screen occupied by master pane
     ratio   = 1/2

     -- Percent of screen to increment by when resizing panes
     delta   = 3/100

------------------------------------------------------------------------

-- WINDOW RULES --
myManageHook = composeAll
    [ className =? "Dunst"                --> doFloat
    , className =? "lxqt-policykit-agent" --> doFloat
    , resource  =? "desktop_window"       --> doIgnore
    , resource  =? "kdesktop"             --> doIgnore ]

------------------------------------------------------------------------

-- Event handling
myEventHook = mempty

------------------------------------------------------------------------

-- STATUS BARS --
myLogHook = return ()

------------------------------------------------------------------------

-- STARTUP HOOK --
myStartupHook = do
	spawnOnce "xwallpaper --zoom ~/Pictures/wallpapers/Wallpapers/0094.png &"
	spawnOnce "picom --experimental-backends &"
	spawnOnce "setxkbmap -layout gb &"
	spawnOnce "lxqt-policykit-agent &"


------------------------------------------------------------------------

-- XMONAD DEFAULTS --
main = do
  xmproc <- spawnPipe "xmobar -x 0 ~/.config/xmobar/xmobarrc"
  xmonad $ docks defaults

------------------------------------------------------------------------

-- CONFIGURATION SETTINGS --
defaults = def {
      -- simple stuff
        terminal           = myTerminal,
        focusFollowsMouse  = myFocusFollowsMouse,
        clickJustFocuses   = myClickJustFocuses,
        borderWidth        = myBorderWidth,
        modMask            = myModMask,
        workspaces         = myWorkspaces,
        normalBorderColor  = myNormalBorderColor,
        focusedBorderColor = myFocusedBorderColor,

      -- key bindings
        keys               = myKeys,
        mouseBindings      = myMouseBindings,

      -- hooks, layouts
        layoutHook         = myLayout,
        manageHook         = myManageHook,
        handleEventHook    = myEventHook,
        logHook            = myLogHook,
        startupHook        = myStartupHook
    }
